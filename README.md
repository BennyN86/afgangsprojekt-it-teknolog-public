# Afgangsprojekt IT-Teknolog

## Problemformulering
Virksomheden, jeg er ansat i som netværksadministrator, står over for en større udvidelse med åbning af nye filialer i London, Paris og Madrid. Hver filial kræver en konfiguration af én MPLS-forbindelse og én site-to-site VPN-forbindelse fra det danske hovedkontor. MPLS anvendes som den primære forbindelse, mens VPN-forbindelsen fungerer som failover. Derudover skal medarbejderne i de nye filialer have tildelt bærbare computere med mulighed for VPN-forbindelse til virksomhedens netværk.

Dette afgangsprojekt har til formål at beskrive og forklare processen for netværksudvidelsen, herunder beskrivelse af anvendte teknologier og uddybning af de trufne valg undervejs. Rapporten skal fungere som dokumentation for virksomhedens netværksstruktur og som en håndbog for fremtidige udvidelser med flere filialer.