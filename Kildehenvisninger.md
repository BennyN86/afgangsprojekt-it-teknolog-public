## Netværksdesign
* https://itconfidence.dk/encyclopedia/network-segmentation/
* https://www.dkits.dk/post/segmenter-dit-netv%C3%A6rk-og-f%C3%A5-st%C3%B8rre-sikkerhed-og-kontrol
* https://youtu.be/PvyEcLhmNBk?si=6HE8ehmbDThrOg4f

## Palo Alto Firewall
* https://docs.paloaltonetworks.com/pan-os/10-2/pan-os-networking-admin/networking
* https://www.youtube.com/playlist?list=PLQQoSBmrXmrw6njwWXSIOiWZE7La8PA5P

## VyOS Routere
* https://docs.vyos.io/en/latest/configuration/index.html

## VMWare ESXi
* https://docs.vmware.com/en/VMware-vSphere/index.html
* https://bluenetsec.com/vmware-port-group-in-trunk-mode/
* https://youtu.be/sx4-0AmFYcg?si=0X03MfZGsOzEv_KP

## NAT
* https://nordvpn.com/da/blog/hvad-er-nat/
* https://www.spiceworks.com/tech/networking/articles/what-is-network-address-translation/
* https://youtu.be/kILDNs4KjYE?si=aAAXYIS6VAbKYhr7

## BGP
* https://docs.vyos.io/en/latest/configuration/protocols/bgp.html
* https://docs.paloaltonetworks.com/pan-os/10-2/pan-os-networking-admin/bgp
* https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA14u0000008UxSCAU&lang=en_US%E2%80%A9&refURL=http%3A%2F%2Fknowledgebase.paloaltonetworks.com%2FKCSArticleDetail
* https://www.youtube.com/watch?v=n4TVBbGW1CY
* https://youtu.be/xSTgb8JLkvs?si=PJfIKL0TiiYQ-cXO
* https://datatracker.ietf.org/doc/html/rfc4271

## OSPF
* https://docs.vyos.io/en/latest/configuration/protocols/ospf.html
* https://youtu.be/pvuaoJ9YzoI?si=78qgEkzqWbM4rBqM

## MPLS
* https://docs.vyos.io/en/latest/configuration/protocols/mpls.html
* https://cibicom.dk/mpls/
* https://loopedback.com/2019/11/21/mpls-a-brief-introduction-and-a-technical-overview-of-mpls-mechanisms-and-terminology-visually-demonstrated/
* https://certesnetworks.com/wp-content/uploads/2019/05/Certes-Networks-The-Truth-About-MPLS-Security-White-Paper.pdf
* https://lev-0.com/2024/01/17/mpls-on-vyos-mpls-basics/
* https://en.wikipedia.org/wiki/Multiprotocol_Label_Switching

## VPN
* https://vpnportalen.dk/hvad-er-ipsec-vpn
* https://www.techtarget.com/searchsecurity/definition/IPsec-Internet-Protocol-Security
* https://docs.paloaltonetworks.com/network-security/ipsec-vpn/administration/set-up-site-to-site-vpn
* https://www.youtube.com/watch?v=GPANrMczTz4
* https://docs.paloaltonetworks.com/network-security/ipsec-vpn/administration/ipsec-vpn-basics

## GlobalProtect
* https://www.paloaltonetworks.com/cyberpedia/what-is-a-remote-access-vpn
* https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000ClHdCAK
* https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000ClFoCAK

## SD-WAN & SASE
* https://www.paloaltonetworks.com/cyberpedia/sd-wan-vs-mpls
* https://www.zscaler.com/zpedia/what-s-difference-between-sd-wan-and-mpls
* https://www.zscaler.com/resources/security-terms-glossary/what-is-sase
* https://www.fortinet.com/resources/cyberglossary/sase

